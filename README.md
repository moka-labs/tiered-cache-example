# Example: Tiered Cache

This example demonstrates how to build a tiered cache (hierarchical cache storages)
using Moka cache with eviction listeners.

This tiered cache consists of two cache storages, or cache tiers:

1. Moka cache as the primary in-memory tier.
2. Redis as the secondary tier.

Also, the tiered cache is configured with time-to-live (TTL) for each entry.

For demonstration purpose, this tiered cache does not use Redis' built-in LRU
eviction policy for the secondary tier. Instead it uses another Moka cache instance
to track the keys in Redis. In a real-world scenario, you may want to share the
cached entries in Redis' across multiple application instances. In such scenario,
this example's secondary tier implementation will not work, and you will need to use
Redis' built-in LRU policy.

Even if in such a scenario, we hope this example will be still useful as it will
illustrate how eviction listeners can be used.

## Table of Contents

- [How to run](#how-to-run)
- [How does it work?](#how-does-it-work)
    - [Case 1. `insert`](#case-1-insert)
    - [Case 2. `get`](#case-2-get)
    - [Case 3. A value evicted from the primary cache due to size constraint](#case-3-a-value-evicted-from-the-primary-cache-due-to-size-constraint)
    - [Case 4. A value evicted from the tracking cache due to size constraint](#case-4-a-value-evicted-from-the-tracking-cache-due-to-size-constraint)
    - [Case 5. A value evicted from the primary cache due to expiration](#case-5-a-value-evicted-from-the-primary-cache-due-to-expiration)
    - [Case 6. A value evicted from the tracking cache due to expiration](#case-6-a-value-evicted-from-the-tracking-cache-due-to-expiration)
- [Limitations on functionalities and efficiencies](#limitations-on-functionalities-and-efficiencies)
    - [A limitation due to simplification of the sample code](#a-limitation-due-to-simplification-of-the-sample-code)
        - [Race condition while updating the tracking cache and Redis](#race-condition-while-updating-the-tracking-cache-and-redis)
    - [Limitations due to current Moka version (v0.9.0)](#limitations-due-to-current-moka-version-v090)
        - [There is a moment a transitioning entry does not exist in both cache tiers](#there-is-a-moment-a-transitioning-entry-does-not-exist-in-both-cache-tiers)
        - [An entry can survive for longer than TTL](#an-entry-can-survive-for-longer-than-ttl)
        - [High memory overhead of the tracking cache](#high-memory-overhead-of-the-tracking-cache)
- [License](#license)

## How to run

Before running the demo, you need to run a Redis server on the localhost. You can use
Docker, for example:

```console
$ docker run -d -p 6379:6379 redis
```

> NOTE: If you want to use Redis server running on a different host and/or listening
> to a different port, edit the following line in `src/main.rs`.
>
> ```rust
> const REDIS_URL: &str = "redis://localhost:6379";
> ```

Then run the example and watch its outputs. You need a recent stable version of Rust
toolchain.

```console
$ cargo run --release
```

## How does it work?

As already mentioned, the tiered cache consists of two cache storages:

1. Moka cache as the primary in-memory storage.
2. Redis as the secondary storage.

and it has another instance of Moka cache to track the keys on Redis. Let's call it
"the tracking cache".

### Case 1. `insert`

When a key-value pair is inserted into the tiered cache, the following happens:

1. Insert the key-value into the primary in-memory cache.
2. Invalidate the key in the tracking cache.
3. If the key exists in the tracking cache, its eviction listener will be called, and
   the listener will remove the key-value from Redis.

### Case 2. `get`

1. Get the value from both the primary in-memory cache and the tracking cache.
    - The reason of using both the caches is that to update the LFU filter in the
      both caches to increase the historical popularity of the key.
2. If the value is found in the primary cache, return it.
3. If the value is found in the tracking cache:
    1. Get the value from Redis.
    2. Insert the key-value into the primary in-memory cache.
    3. Invalidate the key in the tracking cache.
    4. The eviction listener of the tracking cache will be called, and the listener
       will remove the key-value from Redis.

### Case 3. A value evicted from the primary cache due to size constraint

1. The eviction listener of the primary cache will be called.
2. The listener will insert the key into the tracking cache.
3. The listener will set the key-value to Redis.

### Case 4. A value evicted from the tracking cache due to size constraint

1. The eviction listener of the tracking cache will be called.
2. The listener will remove the key-value from Redis.

### Case 5. A value evicted from the primary cache due to expiration

1. The eviction listener of the primary cache will be called.
2. The listener will invalidate the key in the tracking cache (just for sure).
3. If the key exists in the tracking cache, its evection listener will be called, and
   the listener will remove the key-value from Redis.

### Case 6. A value evicted from the tracking cache due to expiration

1. The eviction listener of the tracking cache will be called.
2. The listener will remove the key-value from Redis.

## Limitations on functionalities and efficiencies

### A limitation due to simplification of the sample code

#### Race condition while updating the tracking cache and Redis

For the sake of simplicity, the example does not have lock or something to execute
operations on the tracking cache and Redis in atomic manner. Therefore, there will be
some possible race condition scenarios that can result inconsistency between the
tracking cache and Redis.

If you plan to apply this example to your production code, please consider adding
lock to prevent such the race conditions.

### Limitations due to current Moka version (v0.9.0)

#### There is a moment a transitioning entry does not exist in both cache tiers

In case 3. there is a moment that the entry does not exist in both the primary and
secondary cache tiers. This is because the eviction listener of the primary cache is
called immediately _after_ the entry was evicted.

This can be solved if Moka provides a mode to call the eviction listener just
_before_ the entry will be evicted. A future version of Moka will provide such a
mode.

#### An entry can survive for longer than TTL

When an entry is moved between the cache tiers, its last modified timestamp is reset
to the current time. So if the entry was moved just before the TTL expired, it will
get another full TTL period to live. This is wrong as the entry should be expired
after the TTL past from the insertion to the primary tier.

This can be solved when Moka provides per-entry expiration time. It will allow each
entry to have a wall-clock time for the expiration, rather than the relative time
from the last modified timestamp.

#### High memory overhead of the tracking cache

The memory overhead of the tracking cache might be too high, so the number of entries
that can be stored in Redis will be limited by the application's memory size.

Moka v0.9.0 uses doubly linked list for the LRU policy. Each cache entry has a list
node, and each node has the following information:

1. `usize` pointer to the previous node.
2. `usize` pointer to the next node.
3. `Arc` pointer (`usize`) to the key.
4. `Arc` pointer (`usize`) to the entry info containing last modification timestamp,
   etc.

That is 32 bites per entry on 64-bit platforms.

For managing the external cache storages (especially disk-based storage), this
overhead can be too high. It will be nice if a future version of Moka provides
alternative low-memory footprint LRU policy implementations such as the followings:

- In-memory, set-association-based approximation LRU, which is far more memory
  efficient than the doubly-linked list but sacrificing the accuracy.
- File-backed LRU, which will store chunks of LRU entries in files.

Currently there is no plan to provide such implementations. Please open an issue if
you want such a low-memory footprint LRU policy.

## License

This example is distributed under either of

- The MIT license
- The Apache License (Version 2.0)

at your option.

<!-- See [LICENSE-MIT](LICENSE-MIT) and [LICENSE-APACHE](LICENSE-APACHE) for details. -->
