//! This example demonstrates how to build a tiered cache using Moka cache with
//! eviction listeners. This tiered cache consists of a primary in-memory cache and a
//! secondary Redis-backed cache.
//!
//! We do not use Redis's LRU eviction policy, but instead use Moka to track the keys
//! on Redis and evict them when the cache is full.

use std::{
    sync::{Arc, Mutex},
    thread::sleep,
    time::Duration,
};

use anyhow::Context;
use moka::{notification::RemovalCause, sync::Cache as MokaCache};
use redis::Commands;

type Key = u64;
type Value = String;

#[derive(Debug)]
pub enum CacheTier {
    InMemory,
    Redis,
}

pub struct TieredCache {
    /// Local in-memory cache.
    im_cache: MokaCache<Key, Value>,
    /// A Moka cache to manage the lifecycle of the key-values on Redis.
    redis_cache_tracker: Arc<MokaCache<Key, ()>>,
    /// Redis client.
    redis_client: Arc<Mutex<redis::Client>>,
}

impl TieredCache {
    pub fn new(
        redis_url: &str,
        im_cap: u64,
        redis_cap: u64,
        time_to_live: Duration,
    ) -> anyhow::Result<Self> {
        // ------------------------------------------------------
        // Create a Redis client.
        //
        let redis_client = Arc::new(Mutex::new(redis::Client::open(redis_url)?));

        // ------------------------------------------------------
        // Create the cache tracker for Redis.
        //
        let redis_client1 = Arc::clone(&redis_client);
        let redis_eviction_listener = move |key: Arc<Key>, _value, cause| {
            println!("Evicted from Redis: {}, {:?}", key, cause);

            // If the removal cause matches, remove the value from Redis.
            if matches!(cause, RemovalCause::Size | RemovalCause::Expired | RemovalCause::Explicit) {
                let mut client = redis_client1.lock().unwrap();
                if let Err(e) = Self::delete_from_redis(&mut client, &key) {
                    eprintln!("{}", e);
                }
            }
        };

        let redis_cache_tracker = Arc::new(
            MokaCache::builder()
                .max_capacity(redis_cap)
                .time_to_live(time_to_live)
                .eviction_listener(redis_eviction_listener)
                .build(),
        );

        // ------------------------------------------------------
        // Create the in-memory cache.
        //
        let redis_client2 = Arc::clone(&redis_client);
        let redis_cache_tracker1 = Arc::clone(&redis_cache_tracker);
        let im_cache_eviction_listener = move |key: Arc<Key>, value, cause| {
            println!(
                "Evicted from the in-memory cache: ({}, {}) {:?}",
                key, value, cause
            );

            match cause {
                // If the removal cause is "size", set the value to Redis.
                RemovalCause::Size => {
                    redis_cache_tracker1.insert(*key, ());
                    let mut client = redis_client2.lock().unwrap();
                    if let Err(e) = Self::set_to_redis(&mut client, *key, value) {
                        eprintln!("{}", e);
                    }
                }
                // If the removal cause is "expired", remove the key from the tracker
                // cache (just for sure).
                RemovalCause::Expired => {
                    redis_cache_tracker1.invalidate(&key);
                }
                _ => (),
            }
        };

        let im_cache = MokaCache::builder()
            .max_capacity(im_cap)
            .time_to_live(time_to_live)
            .eviction_listener(im_cache_eviction_listener)
            .build();

        // ------------------------------------------------------
        // Finally create the TieredCache.
        //
        Ok(Self {
            im_cache,
            redis_cache_tracker,
            redis_client,
        })
    }

    /// Inserts key-value pair to the in-memory cache.
    pub fn insert(&self, key: Key, value: Value) {
        // Insert the value to the in-memory cache.
        self.im_cache.insert(key, value);

        // Invalidate the key from the Redis cache tracker. If the key exists, its
        // eviction listener should be called with the removal cause "explicit", and
        // the lister will remove the value from Redis.
        self.redis_cache_tracker.invalidate(&key);
    }

    /// Gets the value for key either from the in-memory cache or from Redis.
    pub fn get(&self, key: &Key) -> Option<(Value, CacheTier)> {
        // We will return the value from the in-memory cache (`im_cache`) if
        // available. But we also need to get from `redis_cache_tracker` no matter if
        // the value available in the in-memory cache. This is necessary to update
        // its LFU filter.
        match (self.im_cache.get(key), self.redis_cache_tracker.get(key)) {
            // Found in the in-memory cache, return it.
            (Some(v), _) => return Some((v, CacheTier::InMemory)),
            // Should be in Redis, get it from Redis and return it.
            (None, Some(_)) => {
                let mut redis = self.redis_client.lock().unwrap();
                if let Ok(Some(v)) = redis.get::<_, Option<String>>(key) {
                    // Release the lock for the Redis client. Otherwise, it will
                    // deadlock when it calls invalidate. This is because its
                    // eviction listener will try to acquire the same lock.
                    std::mem::drop(redis);
                    // Re-insert the value to the in-memory cache.
                    self.im_cache.insert(*key, v.clone());
                    // Invalidate the key from the Redis cache tracker. Its eviction
                    // listener should be called with the removal cause as
                    // "explicit", and the lister will remove the value from Redis.
                    self.redis_cache_tracker.invalidate(key);
                    return Some((v, CacheTier::Redis));
                }
            }
            // Not found in either cache.
            _ => (),
        }

        None
    }

    /// Runs pending 
    pub fn sync(&self) {
        use moka::sync::ConcurrentCacheExt;
        self.im_cache.sync();
        self.redis_cache_tracker.sync();
    }

    fn set_to_redis(client: &mut redis::Client, key: Key, value: Value) -> anyhow::Result<()> {
        client
            .get_connection()
            .with_context(|| "Failed to connect to Redis")?
            .set::<_, _, String>(key, value)
            .with_context(|| format!("Failed to set the value for key {} to Redis", key))?;
        Ok(())
    }

    fn delete_from_redis(client: &mut redis::Client, key: &Key) -> anyhow::Result<()> {
        client
            .get_connection()
            .with_context(|| "Failed to connect to Redis")?
            .del::<_, bool>(*key)
            .with_context(|| format!("Failed to delete the value for key {} from Redis", key))?;
        Ok(())
    }
}

fn main() -> anyhow::Result<()> {
    const REDIS_URL: &str = "redis://localhost:6379";

    const IM_MAX_CAPACITY: u64 = 10;
    const REDIS_MAX_CAPACITY: u64 = 20;
    const TIME_TO_LIVE_SECS: u64 = 10;

    // The number of keys that this demo will insert to the TieredCache.
    const NUM_KEYS: u64 = 40;

    // Create a cache backed by in-memory cache and Redis.
    let cache = TieredCache::new(
        REDIS_URL,
        IM_MAX_CAPACITY,
        REDIS_MAX_CAPACITY,
        Duration::from_secs(TIME_TO_LIVE_SECS),
    )?;

    // Warm up the LFS filter.
    {
        // Put entries more than half the capacity, and call the sync method. This
        // will activate the LFU filter.
        for key in 0..(IM_MAX_CAPACITY / 2 + 1) {
            cache.insert(key, format!("Warm up {}", key));
        }
        cache.sync();

        // Get some of the non-existing keys. This will raise the population of these
        // keys in the LFU filter, so later when we insert keys, they will stay in
        // the in-memory cache when its capacity is exceeded.
        for key in 10..20 {
            cache.get(&key);
        }
    }

    // Insert key-values.
    for key in 0..NUM_KEYS {
        cache.insert(key, format!("Value {}", key));
        println!("Inserted a value for key {}.", key);
    }

    // Ensure housekeeping is done. This is needed for demo purpose to get consistent
    // result. Real applications should not need it.
    cache.sync();

    // Get the values either from in-memory cache or Redis.
    for key in 0..NUM_KEYS {
        let maybe_value = cache.get(&key);
        println!("Got the value for key {} => {:?}.", key, maybe_value);
    }

    // Wait until all keys become expired.
    sleep(Duration::from_secs(TIME_TO_LIVE_SECS + 5));

    // Get the values again but they all should be none now.
    for key in 0..NUM_KEYS {
        let maybe_none = cache.get(&key);
        println!("Got the value for key {} => {:?}.", key, maybe_none);
        assert!(maybe_none.is_none());
    }

    println!("Bye!");

    Ok(())
}
